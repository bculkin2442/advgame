.rdes files
===========
.rdes files are description lists. The abbreviation .rdes stands for "room description"
as that was the original intent of the files, but they were repurposed into general
purpose lists of descriptions. Their format is very simple. It consists of the following
* A integer probability of being chosen. This should be positive, or strange things will
  happen in the internals of the random generator
These are followed by a string description. If you need to split this description onto
multiple lines you can, but if you do the first character of every continuing line must
be a tab.

Multiple descriptions must be seperated by a new line.
The following is an example of a very simple .rdes file

1 This is a simple description

1 This is also a simple description

2 This description is slightly more likely than the others

1 This is a description that wraps onto multiple different lines
	much like this :)


