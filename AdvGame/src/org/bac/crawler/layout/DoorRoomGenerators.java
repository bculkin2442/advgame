package org.bac.crawler.layout;

import org.bac.crawler.layout.api.Direction;
import org.bac.crawler.layout.api.IRoom;
import org.bac.crawler.layout.api.impl.GenLazyRoom;
import org.bac.crawler.layout.api.impl.GenRoom;

public class DoorRoomGenerators {
	public static IRoom createDoorRoom1(Direction d) {
		/*
		 * 10ft passage with t-junction
		 */
		IRoom gr = new GenRoom();

		gr.setRoomDescription("DESCRIBE ME.");

		gr.joinByExit(d.rotateClockwise(),
				new GenLazyRoom(() -> StockGenerators
						.genCorridor(d.rotateClockwise().opposing())),
				"CORRIDOR CONTINUE EXIT", "CORRIDOR CONTINUE ENTRY");

		gr.joinByExit(d.rotateCounterClockwise(),
				new GenLazyRoom(() -> StockGenerators.genCorridor(
						d.rotateCounterClockwise().opposing())),
				"CORRIDOR CONTINUE EXIT", "CORRIDOR CONTINUE ENTRY");
		return gr;
	}

	public static IRoom createDoorRoom2(Direction d) {
		/*
		 * 20ft corridor
		 */
		IRoom gr = new GenRoom();

		gr.setRoomDescription("DESCRIBE ME");

		gr.joinByExit(d.opposing(),
				new GenLazyRoom(() -> StockGenerators.genCorridor(d)),
				"CORRIDOR CONTINUE EXIT", "CORRIDOR CONTINUE ENTRY");

		return gr;
	}
}
