package org.bac.crawler.layout;

import java.util.Collections;
import java.util.Random;

import org.bac.crawler.layout.api.Direction;
import org.bac.crawler.layout.api.IRoom;
import org.bac.crawler.layout.api.impl.GenLazyRoom;
import org.bac.crawler.layout.api.impl.GenRoom;

import bjc.utils.data.GenHolder;
import bjc.utils.funcdata.FunctionalList;

/**
 * Holder class for creating initial rooms
 * 
 * @author ben
 *
 */
public class InitRoomGenerators {
	private static final Random RNG = new Random();

	/**
	 * Create the first initial room
	 * 
	 * @return The first choice for initial room
	 */
	public static IRoom createInitRoom1() {
		/*
		 * 20x20 square, passage on each wall
		 */
		IRoom gr = new GenRoom();

		gr.setRoomDescription(
				"This room is a 20x20 square. DESCRIBE ME MORE.");

		Direction.forCardinalDirections((dir) -> {
			if (RNG.nextBoolean()) {
				gr.addOnewayExit(dir,
						new GenLazyRoom(() -> StockGenerators
								.genCorridor(dir.opposing())),
						"ONEWAY CORRIDOR EXIT DESC.");
			} else {
				gr.joinByExit(dir,
						new GenLazyRoom(() -> StockGenerators
								.genCorridor(dir.opposing())),
						"CORRIDOR EXIT DESC.", "CORRIDOR ENTRY DESC.");
			}
		});

		return gr;
	}

	public static IRoom createInitRoom2() {
		/*
		 * 20x20 square 2 doors and a passage
		 */
		IRoom gr = new GenRoom();

		gr.setRoomDescription("DESCRIBE ME.");

		FunctionalList<Direction> cards = Direction.cardinals();

		Direction dr = cards.randItem(RNG);
		cards.removeMatching(dr);

		Collections.shuffle(cards.getInternal());

		GenHolder<Boolean> onDir = new GenHolder<>(true);

		cards.forEach((dir) -> {
			if (onDir.unwrap(vl -> vl)) {
				onDir.transform(vl -> false);

				if (RNG.nextBoolean()) {
					gr.addOnewayExit(dir,
							new GenLazyRoom(() -> StockGenerators
									.genCorridor(dir.opposing())),
							"ONEWAY CORRIDOR EXIT DESC.");
				} else {
					gr.joinByExit(dir,
							new GenLazyRoom(() -> StockGenerators
									.genCorridor(dir.opposing())),
							"CORRIDOR EXIT DESC.", "CORRIDOR ENTRY DESC.");
				}
			} else {
				gr.joinByExit(dir,
						new GenLazyRoom(
								() -> StockGenerators.genBeyondDoor(dir)),
						"DOOR EXIT DESC", "DOOR ENTRY DESC");
			}
		});

		return gr;
	}
}
