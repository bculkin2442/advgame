package org.bac.crawler.layout;

import java.util.Random;

import org.bac.crawler.layout.api.Direction;
import org.bac.crawler.layout.api.IDirectionAwareGenerator;
import org.bac.crawler.layout.api.IRoom;
import org.bac.crawler.layout.api.IRoomGenerator;
import org.bac.crawler.layout.api.impl.DirectionAwareGenerator;
import org.bac.crawler.layout.api.impl.GenericRoomGenerator;

import bjc.utils.gen.WeightedRandom;

/**
 * Provide several room generators that are commonly used
 * 
 * @author ben
 *
 */
public class StockGenerators {
	private static IRoomGenerator							initRooms;
	private static WeightedRandom<IDirectionAwareGenerator>	corridorWidths;
	private static IDirectionAwareGenerator					doorRooms;

	static {
		initRooms = initializeInitRooms();
		doorRooms = initializeBeyondDoor();

		corridorWidths = initializeCorridorWidths();
	}

	public static IRoom genCorridor(Direction d) {
		return corridorWidths.genVal().generateRoom(d);
	}

	public static IRoom genInitialRoom() {
		return initRooms.generateRoom();
	}

	public static IRoom genBeyondDoor(Direction d) {
		return doorRooms.generateRoom(d);
	}

	private static IDirectionAwareGenerator initializeCorridorsWidth1() {
		DirectionAwareGenerator dag = new DirectionAwareGenerator();

		dag.addRoomType(1, CorridorRoomGenerators::createCorridor1);

		return dag;
	}

	private static WeightedRandom<IDirectionAwareGenerator> initializeCorridorWidths() {
		WeightedRandom<IDirectionAwareGenerator> wr = new WeightedRandom<>(
				new Random());

		wr.addProb(2, initializeCorridorsWidth1());

		return wr;
	}

	private static IRoomGenerator initializeInitRooms() {
		GenericRoomGenerator grg = new GenericRoomGenerator();

		grg.addRoomType(1, InitRoomGenerators::createInitRoom1);

		return grg;
	}

	/*
	 * For now, the only kind of door that exists is unlocked wooden doors
	 * 
	 * Other kinds may be added or not.
	 */
	private static IDirectionAwareGenerator initializeBeyondDoor() {
		DirectionAwareGenerator dag = new DirectionAwareGenerator();

		dag.addRoomType(2, DoorRoomGenerators::createDoorRoom1);

		return dag;
	}
}
