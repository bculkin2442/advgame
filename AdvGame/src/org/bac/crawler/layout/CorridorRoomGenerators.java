package org.bac.crawler.layout;

import org.bac.crawler.layout.api.Direction;
import org.bac.crawler.layout.api.IRoom;
import org.bac.crawler.layout.api.impl.GenRoom;

/**
 * Generator of random corridors
 * 
 * @author ben
 *
 */
public class CorridorRoomGenerators {
	/**
	 * Generate the first choice of corridor
	 * 
	 * @return The first type of corridor
	 */
	public static IRoom createCorridor1(Direction d) {
		IRoom gr = new GenRoom();

		gr.setRoomDescription(
				"A straight 30ft corridor. DESCRIBE ME MORE.");

		gr.joinByExit(d.opposing(), StockGenerators.genCorridor(d),
				"CORRIDOR ENTRY DESC", "CORRIDOR EXIT DESC");
		
		return gr;
	}
}
