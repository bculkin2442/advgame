package org.bac.crawler.layout.api;

import bjc.utils.data.Pair;
import bjc.utils.funcdata.FunctionalList;

public interface IRoomType {
	IDescriptionGenerator getRoomDescriber();
	
	FunctionalList<Pair<Direction, ExitType>> getExits();
	
	String getType();
}
