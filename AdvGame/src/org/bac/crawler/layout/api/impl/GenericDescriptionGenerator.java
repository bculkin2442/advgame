package org.bac.crawler.layout.api.impl;

import org.bac.crawler.layout.api.IDescriptionGenerator;

import bjc.utils.gen.WeightedRandom;

public class GenericDescriptionGenerator implements IDescriptionGenerator {

	private WeightedRandom<String>	descGen;
	private String					type;

	public GenericDescriptionGenerator(WeightedRandom<String> descs,
			String typ) {
		descGen = descs;
		type = typ;
	}

	@Override
	public String describe() {
		return descGen.genVal();
	}

	@Override
	public String type() {
		return type;
	}

}
