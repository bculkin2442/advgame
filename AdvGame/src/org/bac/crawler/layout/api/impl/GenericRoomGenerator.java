package org.bac.crawler.layout.api.impl;

import java.util.Random;
import java.util.function.Supplier;

import org.bac.crawler.layout.api.IRoom;
import org.bac.crawler.layout.api.IRoomGenerator;

import bjc.utils.gen.WeightedRandom;

/**
 * Customizable generator for rooms in dungeons
 * 
 * @author ben
 *
 */
public class GenericRoomGenerator implements IRoomGenerator {
	/**
	 * The generator used internally for picking rooms
	 */
	private WeightedRandom<Supplier<IRoom>> generator;

	/**
	 * Create a new room generator that generates nothing initially
	 */
	public GenericRoomGenerator() {
		generator = new WeightedRandom<>(new Random());
	}

	/**
	 * Add a room to the list of rooms that this generator can generate
	 * 
	 * @param prob
	 *            The probability of picking this particular room. Higher
	 *            is more likey.
	 * @param sup
	 */
	public void addRoomType(int prob, Supplier<IRoom> sup) {
		generator.addProb(prob, sup);
	}

	@Override
	public IRoom generateRoom() {
		Supplier<IRoom> supplier = generator.genVal();

		return new GenLazyRoom(supplier);
	}
}
