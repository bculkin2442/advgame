package org.bac.crawler.layout.api.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.bac.crawler.layout.api.Direction;
import org.bac.crawler.layout.api.IRoom;

import bjc.utils.data.Pair;

/**
 * A second attempt at a room class for generation.
 * 
 * @author ben
 *
 */
public class GenRoom implements IRoom {
	/**
	 * The set of all exits leaving/entering the room
	 */
	private Map<Direction, Pair<String, IRoom>>	exits;

	/**
	 * The description of the room
	 */
	private String								roomDescription;

	/**
	 * Create a new generic room
	 */
	public GenRoom() {
		exits = new HashMap<>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.bac.crawler.layout.v2.api.impl.IRoom#addOnewayExit(org.bac.
	 * crawler.layout.v2.api.Direction,
	 * org.bac.crawler.layout.v2.api.impl.IRoom, java.lang.String)
	 */
	@Override
	public void addOnewayExit(Direction d, IRoom r, String desc) {
		exits.put(d, new Pair<>(desc, r));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.bac.crawler.layout.v2.api.impl.IRoom#exitInDirection(org.bac.
	 * crawler.layout.v2.api.Direction)
	 */
	@Override
	public IRoom exitInDirection(Direction d) {
		return exits.get(d).merge((left, right) -> right);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.bac.crawler.layout.v2.api.impl.IRoom#getExitDescription(org.bac.
	 * crawler.layout.v2.api.Direction)
	 */
	@Override
	public String getExitDescription(Direction d) {
		return exits.get(d).merge((left, right) -> left);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.bac.crawler.layout.v2.api.impl.IRoom#getExitDirections()
	 */
	@Override
	public Set<Direction> getExitDirections() {
		return exits.keySet();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.bac.crawler.layout.v2.api.impl.IRoom#getRoomDescription()
	 */
	@Override
	public String getRoomDescription() {
		return roomDescription;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.bac.crawler.layout.v2.api.impl.IRoom#joinByExit(org.bac.crawler.
	 * layout.v2.api.Direction, org.bac.crawler.layout.v2.api.impl.IRoom,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void joinByExit(Direction d, IRoom r, String enterDesc,
			String exitDesc) {
		this.addOnewayExit(d, r, enterDesc);

		r.addOnewayExit(d.opposing(), this, exitDesc);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.bac.crawler.layout.v2.api.impl.IRoom#setRoomDescription(java.
	 * lang.String)
	 */
	@Override
	public void setRoomDescription(String roomDescription) {
		this.roomDescription = roomDescription;
	}
}
