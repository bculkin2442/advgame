package org.bac.crawler.layout.api.impl;

import org.bac.crawler.layout.api.Direction;
import org.bac.crawler.layout.api.ExitType;
import org.bac.crawler.layout.api.IDescriptionGenerator;
import org.bac.crawler.layout.api.IRoomType;

import bjc.utils.data.Pair;
import bjc.utils.funcdata.FunctionalList;

public class RoomTypeBuilder {
	private static class GenericRoomType implements IRoomType {
		public FunctionalList<Pair<Direction, ExitType>>	exits;
		public IDescriptionGenerator						roomGen;
		public String										type;

		@Override
		public FunctionalList<Pair<Direction, ExitType>> getExits() {
			return exits;
		}

		@Override
		public IDescriptionGenerator getRoomDescriber() {
			return roomGen;
		}

		@Override
		public String getType() {
			return type;
		}
	}

	private GenericRoomType baking;

	public RoomTypeBuilder() {
		baking = new GenericRoomType();
	}

	public void addExit(Direction dir, ExitType typ) {
		baking.exits.add(new Pair<>(dir, typ));
	}

	public IRoomType bake() {
		return baking;
	}

	public String getType() {
		return baking.type;
	}

	public void setDescriber(IDescriptionGenerator idg) {
		baking.roomGen = idg;
	}

	public void setType(String type) {
		baking.type = type;
	}
}
