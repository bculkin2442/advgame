package org.bac.crawler.layout.api.impl;

import java.util.Random;
import java.util.function.Function;

import org.bac.crawler.layout.api.Direction;
import org.bac.crawler.layout.api.IDirectionAwareGenerator;
import org.bac.crawler.layout.api.IRoom;

import bjc.utils.gen.WeightedRandom;

public class DirectionAwareGenerator implements IDirectionAwareGenerator {
	private WeightedRandom<Function<Direction, IRoom>> gens;

	public DirectionAwareGenerator() {
		gens = new WeightedRandom<>(new Random());
	}

	public void addRoomType(int prob, Function<Direction, IRoom> sup) {
		gens.addProb(prob, sup);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.bac.crawler.layout.v2.api.impl.IDirectionAwareGenerator#
	 * generateRoom(org.bac.crawler.layout.v2.api.Direction)
	 */
	@Override
	public IRoom generateRoom(Direction d) {
		Function<Direction, IRoom> gen = gens.genVal();

		return new GenLazyRoom(() -> gen.apply(d));
	}
}
