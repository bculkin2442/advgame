package org.bac.crawler.layout.api.impl;

import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.bac.crawler.layout.api.Direction;
import org.bac.crawler.layout.api.IRoom;

import bjc.utils.funcdata.FunctionalList;

/**
 * A room that waits until it has to to generate a room
 * 
 * @author ben
 *
 */
public class GenLazyRoom implements IRoom {
	/**
	 * Actions that it has been requested performed against the room.
	 * 
	 * The list empties once the room generates and never recieves more
	 * entries after that.
	 */
	private FunctionalList<Consumer<IRoom>>	acts;

	/**
	 * The room to delegate operations to, once it has been generated
	 */
	private IRoom							del	= null;

	/**
	 * The action used to generate the room when necessary
	 */
	private Supplier<IRoom>					src;

	/**
	 * Create a new lazy room with a specified room producer
	 * 
	 * @param sp
	 *            The supplier that creates rooms
	 */
	public GenLazyRoom(Supplier<IRoom> sp) {
		src = sp;

		acts = new FunctionalList<>();
	}

	/**
	 * Stop being lazy and make the actual room
	 */
	private void actualizeRoom() {
		/*
		 * Make the room
		 */
		del = src.get();

		/*
		 * Apply any pending actions to the room
		 */
		acts.forEach((action) -> {
			action.accept(del);
		});

		acts = null;
	}

	@Override
	public void addOnewayExit(Direction d, IRoom r, String desc) {
		if (del == null) {
			acts.add((room) -> {
				room.addOnewayExit(d, r, desc);
			});
		} else {
			del.addOnewayExit(d, r, desc);
		}
	}

	@Override
	public IRoom exitInDirection(Direction d) {
		if (del == null) {
			actualizeRoom();
		}

		return del.exitInDirection(d);
	}

	@Override
	public String getExitDescription(Direction d) {
		if (del == null) {
			actualizeRoom();
		}

		return del.getExitDescription(d);
	}

	@Override
	public Set<Direction> getExitDirections() {
		if (del == null) {
			actualizeRoom();
		}

		return del.getExitDirections();
	}

	@Override
	public String getRoomDescription() {
		if (del == null) {
			actualizeRoom();
		}

		return del.getRoomDescription();
	}

	@Override
	public void joinByExit(Direction d, IRoom r, String enterDesc,
			String exitDesc) {
		if (del == null) {
			acts.add((room) -> {
				room.joinByExit(d, r, enterDesc, exitDesc);
			});
		} else {
			del.joinByExit(d, r, enterDesc, exitDesc);
		}
	}

	@Override
	public void setRoomDescription(String roomDescription) {
		if (del == null) {
			acts.add((room) -> {
				room.setRoomDescription(roomDescription);
			});
		} else {
			del.setRoomDescription(roomDescription);
		}
	}
}
