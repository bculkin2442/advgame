package org.bac.crawler.layout.api;

import java.util.Set;

public interface IRoom {

	/**
	 * Adds a new one-way exit leaving the room
	 * 
	 * @param d
	 *            The direction the exit leaves from
	 * @param r
	 *            The room the exit leaves to
	 * @param desc
	 *            The description of the exit being used
	 */
	void addOnewayExit(Direction d, IRoom r, String desc);

	/**
	 * Retrieve the room that lies beyond an exit in the specified
	 * direction
	 * 
	 * @param d
	 *            The direction to look for an exit in
	 * @return The room beyond the exit in the specified direction
	 */
	IRoom exitInDirection(Direction d);

	/**
	 * Retrieve the description of a exit in a particular direction
	 * 
	 * @param d
	 *            The direction to get a description of the exit for
	 * @return The description of the exit in the specified direction
	 */
	String getExitDescription(Direction d);

	/**
	 * Get a set of all the directions there are exits in
	 * 
	 * @return A set containing all the directions that have valid exits
	 */
	Set<Direction> getExitDirections();

	/**
	 * Get the current description of the room
	 * 
	 * @return The current description of the room
	 */
	String getRoomDescription();

	/**
	 * Join two rooms with a passage between them
	 * 
	 * @param d
	 *            The direction the passage leaves this room
	 * @param r
	 *            The room the passage ends in
	 * @param enterDesc
	 *            The description of the entrance of the passage
	 * @param exitDesc
	 *            The description of the exit of the passage
	 */
	void joinByExit(Direction d, IRoom r, String enterDesc,
			String exitDesc);

	/**
	 * Set the description of the room
	 * 
	 * @param roomDescription
	 *            The new description of the room
	 */
	void setRoomDescription(String roomDescription);

}