package org.bac.crawler.layout.api;

public interface IDirectionAwareGenerator {

	IRoom generateRoom(Direction d);

}