package org.bac.crawler.layout.api;

import java.util.Random;
import java.util.function.Consumer;

import org.apache.commons.lang3.text.WordUtils;

import bjc.utils.funcdata.FunctionalList;

/**
 * A set of cardinal directions
 * 
 * @author ben
 *
 */
public enum Direction {
	DOWN, EAST, NORTH, SOUTH, UP, WEST;

	private static final Random RNG = new Random();

	/**
	 * Get a list of all the cardinal directions
	 * 
	 * @return A list of all the cardinal directions
	 */
	public static FunctionalList<Direction> cardinals() {
		return new FunctionalList<>(NORTH, SOUTH, EAST, WEST);
	}

	/**
	 * Perform the specified action once with each of the cardinal
	 * directions
	 * 
	 * @param act
	 *            The action to perform for each cardinal direction
	 */
	public static void forCardinalDirections(Consumer<Direction> act) {
		cardinals().forEach(act);
	}

	/**
	 * Perform a specified action for a random number of cardinals.
	 * 
	 * @param nCardinals
	 *            The number of cardinal directions to act on. Must be
	 *            greater than 0 and less then 5
	 * @param act
	 *            The action to perform for each of the cardinals
	 */
	public static void forRandomCardinals(int nCardinals,
			Consumer<Direction> act) {
		if (nCardinals <= 0 || nCardinals > 4) {
			throw new IllegalArgumentException(
					"Tried to do things with incorrect number of cardinal directions. Tried with "
							+ nCardinals);
		} else {
			FunctionalList<Direction> cards = cardinals();

			for (int i = 0; i <= 4 - nCardinals; i++) {
				System.out.println("Removing 1 cardinal");

				Direction rDir = cards.randItem(RNG);

				cards.removeMatching(rDir);
			}

			cards.forEach(act);
		}
	}

	/**
	 * Create a value of the enumeration from a string
	 * 
	 * @param val
	 *            The string to turn into a value
	 * @return The direction corresponding to the given value
	 */
	public static Direction properValueOf(String val) {
		return valueOf(val.toUpperCase());
	}

	/**
	 * Get the direction that opposes the current one
	 * 
	 * @return The direction that is in the opposite direction of the
	 *         current one
	 */
	public Direction opposing() {
		switch (this) {
			case NORTH:
				return SOUTH;
			case EAST:
				return WEST;
			case SOUTH:
				return NORTH;
			case WEST:
				return WEST;
			case UP:
				return DOWN;
			case DOWN:
				return UP;
			default:
				throw new IllegalStateException(
						"Enumeration got into a invalid state. WAT");
		}
	}

	/**
	 * Get the direction that is clockwise on the compass from this one.
	 * 
	 * Only works on cardinals.
	 * 
	 * @return The cardinal clockwise from this one
	 */
	public Direction rotateClockwise() {
		switch (this) {
			case NORTH:
				return EAST;
			case EAST:
				return SOUTH;
			case SOUTH:
				return WEST;
			case WEST:
				return NORTH;
			case UP:
			case DOWN:
			default:
				throw new UnsupportedOperationException(
						"Can't rotate non-cardinal direction " + this);

		}
	}

	/**
	 * Get the direction that is counter-clockwise on the compass from this
	 * one.
	 * 
	 * Only works on cardinals.
	 * 
	 * @return The cardinal counter-clockwise from this one
	 */
	public Direction rotateCounterClockwise() {
		switch (this) {
			case NORTH:
				return WEST;
			case EAST:
				return NORTH;
			case SOUTH:
				return EAST;
			case WEST:
				return SOUTH;
			case UP:
			case DOWN:
			default:
				throw new UnsupportedOperationException(
						"Can't rotate non-cardinal direction " + this);

		}
	}

	@Override
	public String toString() {
		return WordUtils.capitalize(super.toString().toLowerCase());
	}
}
