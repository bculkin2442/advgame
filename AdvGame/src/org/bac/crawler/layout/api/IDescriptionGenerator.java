package org.bac.crawler.layout.api;

/**
 * Generates descriptions for a specific type of object
 * 
 * @author ben
 *
 */
public interface IDescriptionGenerator {
	/**
	 * Create a description of the type this generator provides
	 * 
	 * @return A description of the type this generator describes
	 */
	String describe();

	/**
	 * Get the type this generator describes
	 * 
	 * @return The type of description this generator generates
	 */
	String type();
}
