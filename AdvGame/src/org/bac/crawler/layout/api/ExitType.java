package org.bac.crawler.layout.api;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import org.bac.crawler.layout.api.impl.GenericDescriptionGenerator;
import org.bac.crawler.layout.parsers.DescriptionFileParser;

/**
 * Represents all the possible ways to leave a room.
 * 
 * @author ben
 *
 */
public enum ExitType {
	DOOR, PASSAGE, STAIRS, WELL;

	private static Map<ExitType, IDescriptionGenerator> gens;

	static {
		gens = new HashMap<>();

		try {
			gens.put(DOOR, new GenericDescriptionGenerator(
					DescriptionFileParser.parseFile(
							new FileInputStream("DOORDESC FILE HERE")),
					"doors"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get a description generator that describes an exit
	 * 
	 * @return
	 */
	public IDescriptionGenerator getDescriber() {
		return gens.get(this);
	}

	/**
	 * Create a value of the enumeration from a string
	 * 
	 * @param val
	 *            The string to turn into a value
	 * @return The direction corresponding to the given value
	 */
	public static ExitType properValueOf(String val) {
		return valueOf(val.toUpperCase());
	}
}
