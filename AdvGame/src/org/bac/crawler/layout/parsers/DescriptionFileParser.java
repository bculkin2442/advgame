package org.bac.crawler.layout.parsers;

import java.io.InputStream;

import bjc.utils.data.Pair;
import bjc.utils.funcdata.FunctionalStringTokenizer;
import bjc.utils.gen.WeightedRandom;
import bjc.utils.parserutils.RuleBasedConfigReader;

public class DescriptionFileParser {
	private static RuleBasedConfigReader<DescriptionState> reader;

	static {
		reader = new RuleBasedConfigReader<>(
				DescriptionFileParser::startDescription,
				DescriptionFileParser::continueDescription,
				DescriptionFileParser::endDesc);
	}

	public static WeightedRandom<String> parseFile(InputStream is) {
		return reader.fromStream(is, new DescriptionState()).getResults();
	}

	private static void startDescription(FunctionalStringTokenizer fst,
			Pair<String, DescriptionState> par) {
		int prob = Integer.parseInt(par.merge((left, right) -> left));

		String desc = fst.toList((s) -> s).reduceAux("", (t, u) -> t + u,
				t -> t);

		par.doWith((left, right) -> {
			right.startDesc(prob, desc);
		});
	}

	private static void continueDescription(FunctionalStringTokenizer fst,
			DescriptionState stat) {
		String desc = fst.toList((s) -> s).reduceAux("", (t, u) -> t + u,
				t -> t);

		stat.continueDesc(desc);
	}

	private static void endDesc(DescriptionState stat) {
		stat.endDesc();
	}
}
