package org.bac.crawler.layout.parsers;

import java.io.FileInputStream;
import java.io.InputStream;

import org.bac.crawler.layout.api.Direction;
import org.bac.crawler.layout.api.ExitType;
import org.bac.crawler.layout.api.IRoomType;

import bjc.utils.data.GenHolder;
import bjc.utils.data.Pair;
import bjc.utils.funcdata.FunctionalStringTokenizer;
import bjc.utils.parserutils.RuleBasedConfigReader;
import bjc.utils.parserutils.UnknownPragmaException;

public class RoomTypeFileParser {
	private static RuleBasedConfigReader<RoomTypeState> reader;

	static {
		reader = new RuleBasedConfigReader<>(RoomTypeFileParser::startType,
				RoomTypeFileParser::continueType,
				RoomTypeFileParser::endType);

		reader.addPragma("random-dirs",
				RoomTypeFileParser::addDirectionAlias);
	}

	private static void addDirectionAlias(FunctionalStringTokenizer fst,
			RoomTypeState stat) {

		int nDirs = Integer.parseInt(fst.nextToken());

		GenHolder<Integer> cardCounter = new GenHolder<>(1);

		Direction.forRandomCardinals(nDirs, (dir) -> {
			stat.addDirectionAlias("rand-" + cardCounter.unwrap(vl -> vl),
					dir);

			cardCounter.transform(vl -> vl + 1);
		});
	}

	private static void continueType(FunctionalStringTokenizer fst,
			RoomTypeState stat) {
		throw new UnsupportedOperationException(
				"Room type commands may not be continued");
	}

	private static void endType(RoomTypeState stat) {
		// Room type commands aren't continued, so nothing needs to be done
		// here
	}

	public static IRoomType parseRoomTypeDesc(InputStream is) {
		return reader.fromStream(is, new RoomTypeState()).getType();
	}

	private static void startType(FunctionalStringTokenizer fst,
			Pair<String, RoomTypeState> par) {
		switch (par.merge((left, right) -> left)) {
			case "name":
				par.doWith(
						(left, right) -> right.setType(fst.nextToken()));
				break;

			case "descriptions":
				par.doWith((left, right) -> {
					try {
						right.setDescriber(DescriptionFileParser.parseFile(
								new FileInputStream(fst.nextToken())));
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
				break;

			case "exit":
				par.doWith((left, right) -> {
					right.addExit(right.getDirection(fst.nextToken()),
							ExitType.properValueOf(fst.nextToken()));
				});
				break;

			default:
				throw new UnknownPragmaException(
						"Unknown room type command "
								+ par.merge((left, right) -> left));
		}
	}
}
