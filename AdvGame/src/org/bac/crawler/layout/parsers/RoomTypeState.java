package org.bac.crawler.layout.parsers;

import java.util.HashMap;
import java.util.Map;

import org.bac.crawler.layout.api.Direction;
import org.bac.crawler.layout.api.ExitType;
import org.bac.crawler.layout.api.IRoomType;
import org.bac.crawler.layout.api.impl.GenericDescriptionGenerator;
import org.bac.crawler.layout.api.impl.RoomTypeBuilder;

import bjc.utils.gen.WeightedRandom;

public class RoomTypeState {
	private RoomTypeBuilder			build;

	private Map<String, Direction>	aliases;

	public RoomTypeState() {
		aliases = new HashMap<>();
	}

	public void addDirectionAlias(String alias, Direction dir) {
		aliases.put(alias, dir);
	}

	public void addExit(Direction dr, ExitType ty) {
		build.addExit(dr, ty);
	}

	public Direction getDirection(String dirName) {
		if (aliases.containsKey(dirName)) {
			return aliases.get(dirName);
		} else {
			return Direction.properValueOf(dirName);
		}
	}

	public IRoomType getType() {
		return build.bake();
	}

	public void setType(String type) {
		build.setType(type);
	}

	public void setDescriber(WeightedRandom<String> descGen) {
		build.setDescriber(
				new GenericDescriptionGenerator(descGen, build.getType()));
	}

	public RoomTypeBuilder getBuilder() {
		return build;
	}
}
