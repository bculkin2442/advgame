package org.bac.crawler;

import java.util.Scanner;

import org.bac.crawler.layout.StockGenerators;
import org.bac.crawler.layout.api.Direction;
import org.bac.crawler.layout.api.IRoom;

/**
 * A skeleton class for walking through dungeons
 * 
 * @author ben
 *
 */
public class RoomExplorer {
	/**
	 * Displays all of the possible directions to exit a room in
	 * 
	 * @param r
	 *            The room to display exits from
	 */
	private static void displayExits(IRoom r) {
		System.out.println(
				"Peering around for exits, you see possible exits in "
						+ "each of the following directions:");

		/*
		 * List each of the directions that has an exit
		 */
		for (Direction d : r.getExitDirections()) {
			System.out.println("\t" + d.toString());
		}

		System.out.println();
	}

	/**
	 * Display the help message for the explorer
	 */
	private static void displayHelp() {
		System.out.println("Type look to re-examine the room, "
				+ " look exits to look at possible exits,"
				+ " help to display this message,"
				+ " look <direction> to look at an exit,"
				+ " and go <direction> to go through an exit.");
		System.out.println();
	}

	/**
	 * Display the intro text for starting the dungeon
	 * 
	 * @param r
	 *            The room to start the dungeon in.
	 */
	private static void doIntro(IRoom r) {
		System.out.println(
				"You awaken in a room, unsure of how you got here,"
						+ " or what you were doing beforehand.\n");

		System.out.println("You look around and see the following:\n"
				+ r.getRoomDescription() + "\n");

		displayExits(r);
	}

	/**
	 * Display the results of going in a particular direction from a
	 * particular room
	 * 
	 * @param r
	 *            The room to leave from.
	 * @param dir
	 *            The direction to leave in.
	 * @return The room you are now in.
	 */
	private static IRoom goDirection(IRoom r, String dir) {
		/*
		 * Get the direction to proceed in
		 */
		Direction d = Direction.properValueOf(dir);

		/*
		 * Make sure we're going a valid way
		 */
		if (r.getExitDirections().contains(d)) {
			IRoom rm = r.exitInDirection(d);

			/*
			 * Go into the new room
			 */
			System.out.println("You head " + d + " through the exit,"
					+ " and see the following on the other side:");
			System.out.println(rm.getRoomDescription());

			displayExits(rm);

			return rm;
		} else {
			/*
			 * Tell the user they're doing something silly.
			 */
			System.out.println("You think about walking "
					+ d.toString().toLowerCase()
					+ ", but that doesn't seem like a productive use of time.");

			return r;
		}
	}

	/**
	 * Handle the user entering an incorrect command
	 * 
	 * @param initRoom
	 *            The room we're currently in.
	 * @param parts
	 *            The command that was incorrect.
	 */
	private static void handleIncorrectCommand(IRoom initRoom,
			String[] parts) {
		/*
		 * Tell the user the command was incorrect.s
		 */
		System.out.println("I don't understand that.");

		/*
		 * Suggest which command they should use
		 */
		switch (parts[0].toLowerCase()) {
			case "exit":
				System.out.println(
						"Perhaps you meant to say \"quit\" instead, to leave the games.");
		}

		System.out.println();
	}

	/**
	 * Look in a specific direction in a room
	 * 
	 * @param initRoom
	 *            The room you are looking from from
	 * @param parts
	 *            The parts of the command that was entered
	 */
	private static void handleLookCommand(IRoom initRoom,
			String[] parts) {
		/*
		 * Handle re-examing the current room.
		 */
		if (parts.length == 1) {
			System.out.println("You re-examine your surroundings and"
					+ " see the following:\n "
					+ initRoom.getRoomDescription());
			System.out.println();
		} else {
			/*
			 * Handle the user wanting to look at the exits in general.
			 */
			if (parts[1].equalsIgnoreCase("exits")) {
				displayExits(initRoom);
			} else {
				/*
				 * Handle the user wanting to look at a specific exit
				 */
				lookDirection(initRoom, parts[1]);
			}
		}
	}

	/**
	 * Handle looking at a specific exit/object in a room.
	 * 
	 * @param r
	 *            The room to look from
	 * @param dir
	 *            The direction/object to look in/ats
	 */
	private static void lookDirection(IRoom r, String dir) {
		/*
		 * Get the direction being looked at.
		 * 
		 * TODO make sure this handles objects if they get added
		 */
		Direction d = Direction.properValueOf(dir);

		/*
		 * Make sure the user is looking at something that exists
		 */
		if (r.getExitDirections().contains(d)) {
			/*
			 * Tell the user what they see.
			 */
			System.out.println(
					"You look " + d + " and this is what you see:");
			System.out.println(r.getExitDescription(d));
		} else {
			/*
			 * Tell the user they did a silly thing.
			 */
			System.out.println("You stare at the wall for several seconds,"
					+ " but nothing happens.");
		}

		System.out.println();
	}

	/**
	 * Run the main crawler
	 * 
	 * @param args
	 *            Ignored CLI arguments
	 */
	public static void main(String[] args) {
		/*
		 * Build the dungeon
		 */
		// GenRoom initRoom = buildRoom();
		IRoom initRoom = StockGenerators.genInitialRoom();

		/*
		 * Do the intro for the dungeon
		 */
		doIntro(initRoom);

		/*
		 * Run the command loop to drive the crawler
		 */
		runCommandLoop(initRoom);
	}

	/**
	 * Run the command loop that drives the system
	 * 
	 * @param initRoom
	 *            The room to start the crawler in
	 */
	private static void runCommandLoop(IRoom initRoom) {
		/*
		 * Remind them how to access help
		 */
		System.out.println("NOTE: Type help to see possible commands.");

		System.out.print("> ");

		/*
		 * Get the command and do initial parsing
		 */
		Scanner sc = new Scanner(System.in);

		String[] parts = sc.nextLine().split(" ");

		System.out.println();

		/*
		 * Handle parsing commands
		 */
		while (parts[0] != "quit") {

			switch (parts[0].toLowerCase()) {
				/*
				 * Handle going places
				 */
				case "go":
					initRoom = goDirection(initRoom, parts[1]);
					break;

				/*
				 * Handle displaying help
				 */
				case "help":
					displayHelp();
					break;

				/*
				 * Handle looking at things
				 */
				case "look":
					handleLookCommand(initRoom, parts);
					break;

				/*
				 * Handle invalid commands
				 */
				default:
					handleIncorrectCommand(initRoom, parts);
			}

			System.out.print("> ");

			/*
			 * Get the next commands
			 */
			parts = sc.nextLine().split(" ");

			System.out.println();
		}

		/*
		 * Clean up after ourselves
		 */
		sc.close();
	}
}
